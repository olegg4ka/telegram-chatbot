<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin,Content-Type,Accept,Authorization,X-Features');
header('Access-Control-Allow-Credentials: true');

use Application\Application;
use Application\Config\SentryTracer;
use Core\Component\Container\Container;
use DI\ContainerBuilder;

define('ROOT_PATH', __DIR__ . '/../');
define('CORE_PATH', __DIR__ . '/../src/Core/');

require ROOT_PATH . 'vendor/autoload.php';
\Sentry\init(['dsn' => (new SentryTracer())->getDsnForSentry()]);
$containerBuilder = new ContainerBuilder;
$containerBuilder->addDefinitions('../src/Application/Config/dependencies.php');
try {
	$container = $containerBuilder->build();
	Container::init($container);
	Application::getInstance();
} catch (Exception $e) {
	Sentry\captureException($e);
	$logger = Container::get('LoggerStartApp');
	$logger->info($e->getMessage() . ' Line: ' . $e->getLine() . '. Code: ' . $e->getCode() . '.', []);
}


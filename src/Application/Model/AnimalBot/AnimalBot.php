<?php

namespace Application\Model\AnimalBot;

use Application\Model\AnimalBot\Command\CheckAnimalFood;
use Application\Model\AnimalBot\Command\Command;
use Application\Model\AnimalBot\Common\ProductType\ProductType;
use Core\Backend\Component\Messenger\Telegram;
use Core\Backend\Component\User\UserInterface;
use Core\Backend\Utils\UserType\UserType;
use Core\Component\Container\Container;
use Core\Component\RabbitMQ\RabbitMQ;
use Core\DataBaseRequest\CheckUser\CheckUser;
use Core\DataBaseRequest\GetCommand\GetCommand;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Telegram\Bot\Exceptions\TelegramSDKException;
use Telegram\Bot\FileUpload\InputFile;
use Telegram\Bot\Keyboard\Keyboard;

/**
 *
 */
class AnimalBot
{

	private $telegram;

	/**
	 * @throws TelegramSDKException
	 */
	public function __construct()
	{
		$this->telegram = (new Telegram())->getTelegram();
	}

	/**
	 * @return void
	 * @throws GuzzleException
	 * @throws TelegramSDKException
	 */
	public function execute()
	{

		echo 'Скріпт виповнився, але результат роботи його не видно!';

		$update = $this->telegram->getWebhookUpdates();

		/**
		 * callback_query - если у кнопки есть что возвращать в callback_data
		 */
		if (isset($update['callback_query'])) {
			$result = $update['callback_query'];
			$text = $result['data'];
			$param = $result['data'];
			$result['message']['text'] = $result['data'];
			$message = $result['message'];
		} else if (isset($update['my_chat_member'])) {
			$result['message'] = $update['my_chat_member'];
			$message = $result['message'];
			$message['message_id'] = $update['update_id'];
			$text = '-';
		}else {
			$result = $update;
			$text = $result['message']['text'];
			$message = $result['message'];
			$param = '-';
		}
		unset($result['message']['reply_markup']);
		$message['botId'] = $this->telegram->getMe()->toArray()['id'];
		$chat_id = $result['message']['chat']['id'];
		$contact = $result['message']['contact'];

		if (isset($message['document'])) {
			$message['text'] = 'document';
			$text = $message['text'];
		}

		if ($chat_id){
			Container::get(UserInterface::class)->setUserID($chat_id);
		}else{
			Container::get(UserInterface::class)->setUserID(1);
		}


		if ($text == 'setFilterYes') {
			$param = 'Yes';
		} else if ($text == 'setFilterNo') {
			$param = 'No';
		}

		$command = 'defaultCommand';
		if (isset($text)) {
			$command = (new Command($text))->getCommand();
		} else if (isset($contact)) {
			$command = 'enterRegister';
			$text = 'enterRegister';
			unset($message['reply_to_message']);
		}

		$checkUser = CheckUser::checkUser($message);

		switch ($command) {
			case 'document':
				$param = 'document';
				$res = GetCommand::getRequestCommand($command, $param, $message);
				$document = $this->telegram->getFile(['file_id' => $message['document']['file_id']]);
				$client = new Client();
				$url = Container::get('config')['api']['telegram']['saler-url'];
				if ($checkUser[0]['userTypeID'] == UserType::volonter) {
					$url = Container::get('config')['api']['telegram']['volonter-url'];
				}
				$response = $client->request('GET', $url,
					[
						'headers' => [
							'content-type' => 'application/json',
							'Accept' => 'application/json'
						],
						'body' => json_encode([
							'telegramId' => $chat_id,
							'file_id' => $message['document']['file_id'],
							'file_name' => $message['document']['file_name'],
							'file_path' => $document['file_path']
						]),
					]
				);
				if ($response->getStatusCode() == 200) {
					$text = 'Дякуємо за участь в проекті. Ваш файл поставлено в чергу на обробку.';
					$res = GetCommand::getRequestCommand('Exit', '-', $message);
				} else {
					$text = 'Помилка серверу. Спробуйте будь ласка пізніше.';
				}
				$this->telegram->sendMessage([
					'chat_id' => $chat_id,
					'text' => $text,
					'reply_markup' => self::renderExitKeyboard()
				]);
				break;
			case 'getExample':
				$res = GetCommand::getRequestCommand($command, $param, $message);
				$fileName = 'Example.xlsx';
				if ($checkUser[0]['userTypeID'] == UserType::volonter) {
					$fileName = 'Example-Declar.xlsx';
				}
				$content = (new InputFile())->setFile($fileName)->setFilename($fileName);
				$this->telegram->sendDocument([
					'chat_id' => $chat_id,
					'document' => $content,
				]);
				break;
			case 'setFile':
				$res = GetCommand::getRequestCommand($command, $param, $message);
				$this->telegram->sendMessage([
					'chat_id' => $chat_id,
					'text' => 'Будь ласка перетягніть файл в чат бот.'
				]);
				break;
			case 'documentInfo':
				$markup = Keyboard::make([
					'inline_keyboard' => self::renderButtonsSelect(
						'getExample',
						'setFile',
						'Отримати приклад!',
						'Завантажити файл!'
					),
					'resize_keyboard' => true,
					'one_time_keyboard' => false
				]);
				$this->telegram->sendMessage([
					'chat_id' => $chat_id,
					'text' => 'Отримайте приклад та загрузіть файл. ' . "\n" .
						'Зауважте, що потрібно заповнити свій файл по прикладу!',
					'reply_markup' => $markup
				]);
				break;
			case 'Help':
				$this->telegram->sendMessage([
					'chat_id' => $chat_id,
					'parse_mode' => 'HTML',
					'text' => 'Команди для переліку <b> /start, /help </b>'
				]);
				break;
			case 'Start':
				$res = GetCommand::getRequestCommand($command, $param, $message);
				$markup = Keyboard::make([
					'inline_keyboard' => self::renderTwoCollKeyboard($res),
					'resize_keyboard' => true,
					'one_time_keyboard' => false,
				]);
				$this->telegram->sendMessage([
					'chat_id' => $chat_id,
					'text' => 'Бот створений для підтримки власників тварин. Він допоможе здійснити пошук необхідних або 
					продаж власних товарів\послуг, або оформити заявку на отримання благодійної допомоги.',
					'reply_markup' => $markup
				]);
				break;
			case 'Helping':
				$res = GetCommand::getRequestCommand($command, $param, $message);
				if ($checkUser) {
					if ($checkUser[0]['IsPhone'] == 1) {
						$markup = Keyboard::make([
							'inline_keyboard' => self::renderTwoCollKeyboard($res),
							'resize_keyboard' => true,
							'one_time_keyboard' => false
						]);
						$this->telegram->sendMessage([
							'chat_id' => $chat_id,
							'text' => 'Оберіть регіон',
							'reply_markup' => $markup
						]);
					} else {
						$markup = Keyboard::make([
							'keyboard' => [[
								Keyboard::button([
									'text' => 'Надати номер телефону',
									'resize_keyboard' => true,
									'one_time_keyboard' => true,
									'request_contact' => true])
							]],
							'resize_keyboard' => true,
						]);
						$this->telegram->sendMessage([
							'chat_id' => $chat_id,
							'text' => 'Будь ласка, найдайте свій номер телефону ' . "\n\n" . ' Зобов`язуємося зберігати 
							конфіденційність особистих даних.',
							'reply_markup' => $markup
						]);
					}
				}
				break;
			case 'Volonter':
				$res = GetCommand::getRequestCommand($command, $param, $message);
				$markup = Keyboard::make([
					'inline_keyboard' => self::renderButtonsSelect(
						'isExitYes',
						'isExitNo',
						'Вихід',
						'Продовжити'
					),
					'resize_keyboard' => true,
					'one_time_keyboard' => false
				]);
				$this->telegram->sendMessage([
					'chat_id' => $chat_id,
					'text' => 'Отримати благодійну допомогу можуть тільки сеціалізовані установи (зоопарки, приюти, тощо).
					Для отримання допомоги потрібно зареєструватись та оформити заявку',
					'reply_markup' => $markup
				]);
				break;
			case 'Search':
				$res = GetCommand::getRequestCommand($command, $param, $message);
				$markup = Keyboard::make([
					'inline_keyboard' => self::renderTwoCollKeyboard($res),
					'resize_keyboard' => true,
					'one_time_keyboard' => false
				]);
				$this->telegram->sendMessage([
					'chat_id' => $chat_id,
					'text' => 'Оберіть регіон',
					'reply_markup' => $markup
				]);
				break;
			case 'Continue':
				{
					$res = GetCommand::getRequestCommand($command, $param, $message);
					$markup = Keyboard::make([
						'inline_keyboard' => self::renderTwoCollKeyboard($res),
						'resize_keyboard' => true,
						'one_time_keyboard' => false
					]);
					$this->telegram->sendMessage([
						'chat_id' => $chat_id,
						'text' => 'Оберіть регіон',
						'reply_markup' => $markup
					]);
				}
				break;
			case 'enterRegister':
				{
					/**
					 * Команда реєстрації
					 */
					$res = GetCommand::getRequestCommand($command, $param, $message);
					if ($res) {
						$markup = Keyboard::make([
							'inline_keyboard' => self::renderTwoCollKeyboard($res),
							'resize_keyboard' => true,
							'one_time_keyboard' => false
						]);
						$text = 'Ваші дані внесено. Дякую.' . "\n\n" . 'Оберіть регіон дії вашої діяльності.';
					} else {
						$text = 'Реєстрація не можлива.';
						$markup = json_encode(array(
							'remove_keyboard' => true
						));
					}
					$this->telegram->sendMessage([
						'chat_id' => $chat_id,
						'text' => $text,
						'reply_markup' => $markup
					]);
				}
				break;
			case 'setRegion':
				/**
				 * Команди регіонів
				 */
				$res = GetCommand::getRequestCommand($command, $param, $message);
				if ($checkUser) {
					if ($checkUser[0]['userTypeID'] == UserType::searcher) {
						$markup = Keyboard::make([
							'inline_keyboard' => self::renderTwoCollKeyboard($res),
							'resize_keyboard' => true,
							'one_time_keyboard' => false
						]);
						$this->telegram->sendMessage([
							'chat_id' => $chat_id,
							'text' => "Оберіть вид тварини. " . "\xF0\x9F\x98\xBC \xF0\x9F\x90\xB6",
							'reply_markup' => $markup
						]);
					} else {
						if ($checkUser[0]['isContact'] == 1) {
							$this->telegram->sendMessage([
								'chat_id' => $chat_id,
								'parse_mode' => 'HTML',
								'text' => 'Внесіть будь ласка свої контактні дані, для зв`язку з шукачами' . "\n\n" .
									'<b>Ця інформація буде надана шукачу!</b>',
							]);
						} else {
							if ($checkUser[0]['userTypeID'] == UserType::saler) {
								$buttons = self::renderButtonsSelect(
									'exelYes',
									'exelNo',
									'Додати',
									'Ввести вручну'
								);
							} else {
								$buttons = self::renderButtonsSelect(
									'exelVolonterYes',
									'exelVolonterNo',
									'Надіслати заявку',
									'Сформувати вручну'
								);
							}
							$markup = Keyboard::make([
								'inline_keyboard' => $buttons,
								'resize_keyboard' => true,
								'one_time_keyboard' => false
							]);
							$this->telegram->sendMessage([
								'chat_id' => $chat_id,
								'text' => 'Бажаєте завантажити файл або ввести вручну?',
								'reply_markup' => $markup
							]);
						}
					}
				}
				break;
			case 'setRegionContinue':
				$res = GetCommand::getRequestCommand($command, $param, $message);
				$markup = Keyboard::make([
					'inline_keyboard' => self::renderTwoCollKeyboard($res),
					'resize_keyboard' => true,
					'one_time_keyboard' => false
				]);
				$this->telegram->sendMessage([
					'chat_id' => $chat_id,
					'text' => 'Оберіть вид тварини. ' . "\xF0\x9F\x98\xBC \xF0\x9F\x90\xB6",
					'reply_markup' => $markup
				]);
				break;
			case 'setAnimal':
				/**
				 * Команди тварин
				 */
				$res = GetCommand::getRequestCommand($command, $param, $message);
				if ($checkUser) {
					$markup = Keyboard::make([
						'inline_keyboard' => self::renderTwoCollKeyboard($res),
						'resize_keyboard' => true,
						'one_time_keyboard' => false
					]);
					$this->telegram->sendMessage([
						'chat_id' => $chat_id,
						'reply_markup' => $markup,
						'text' => 'Оберіть вид товару',
					]);
				} else {
					$this->telegram->sendMessage([
						'chat_id' => $chat_id,
						'text' => 'Проблема з юзером.',
					]);
				}
				break;
			case 'setProductType':
				$res = GetCommand::getRequestCommand($command, $param, $message);
				if ($checkUser) {
					if ($checkUser[0]['userTypeID'] == UserType::searcher) {
						$markup = Keyboard::make([
							'inline_keyboard' => self::renderButtonsSelect(
								'setFilterYes',
								'setFilterNo',
								'Розширений пошук',
								'Спрощений пошук'
							),
							'resize_keyboard' => true,
							'one_time_keyboard' => false
						]);
						$this->telegram->sendMessage([
							'chat_id' => $chat_id,
							'text' => 'Бажаєте здійснити пошук або деталізувати умови відбору',
							'reply_markup' => $markup,
						]);
					} else {
						$markup = Keyboard::make([
							'inline_keyboard' => self::renderTwoCollKeyboard($res),
							'resize_keyboard' => true,
							'one_time_keyboard' => false
						]);
						switch ($checkUser[0]['ProductType']) {
							case ProductType::Food :
								$text = 'Вкажіть для якого типу корму.';
								break;
							case ProductType::VetDrug :
								$text = 'Вкажіть для чого вет. препарат.';
								break;
							case ProductType::Other:
								$text = 'Вкажіть назву товару.';
								break;
						}
						$this->telegram->sendMessage([
							'chat_id' => $chat_id,
							'reply_markup' => $markup,
							'text' => $text,
						]);
					}
				}
				break;
			case 'setProductSubType':
				$res = GetCommand::getRequestCommand($command, $param, $message);
				if ($checkUser) {
					$markup = Keyboard::make([
						'inline_keyboard' => self::renderTwoCollKeyboard($res),
						'resize_keyboard' => true,
						'one_time_keyboard' => false
					]);
					switch ($checkUser[0]['ProductType']) {
						case ProductType::Food :
							$text = 'Вкажіть для якого віку або розміру корму.';
							break;
						case ProductType::VetDrug :
							$text = 'Вкажіть для чого вет. препарат.';
							break;
						case ProductType::Other:
							$text = 'Вкажіть назву товару.';
							break;
					}
					$this->telegram->sendMessage([
						'chat_id' => $chat_id,
						'reply_markup' => $markup,
						'text' => $text
					]);
				}
				break;
			case 'setAnimalGroup':
				$res = GetCommand::getRequestCommand($command, $param, $message);
				$markup = Keyboard::make([
					'inline_keyboard' => self::renderTwoCollKeyboard($res),
					'resize_keyboard' => true,
					'one_time_keyboard' => false
				]);
				$this->telegram->sendMessage([
					'chat_id' => $chat_id,
					'reply_markup' => $markup,
					'text' => 'Вкажіть породу вашої тварини.',
				]);
				break;
			case 'setBreed':
				$res = GetCommand::getRequestCommand($command, $param, $message);
				$markup = Keyboard::make([
					'inline_keyboard' => self::renderThreeCollKeyboard($res),
					'resize_keyboard' => true,
					'one_time_keyboard' => false
				]);
				$this->telegram->sendMessage([
					'chat_id' => $chat_id,
					'reply_markup' => $markup,
					'text' => 'Вкажіть вид корму для вашої тварини.'
				]);
				break;
			case 'setFilter':
				if ($param == 'setFilterYes') {
					$param = 'Yes';
				} else if ($param == 'setFilterNo') {
					$param = 'No';
				}
				$res = GetCommand::getRequestCommand($command, $param, $message);
				if ($checkUser) {
					if ($param == 'Yes') {
						$markup = Keyboard::make([
							'inline_keyboard' => self::renderTwoCollKeyboard($res),
							'resize_keyboard' => true,
							'one_time_keyboard' => false
						]);
						switch ($checkUser[0]['ProductType']) {
							case ProductType::Food :
								$text = 'Вкажіть для якого типу корму.';
								break;
							case ProductType::VetDrug :
								$text = 'Вкажіть для чого вет. препарат.';
								break;
							case ProductType::Other:
								$text = 'Вкажіть назву товару.';
								break;
						}
						$this->telegram->sendMessage([
							'chat_id' => $chat_id,
							'reply_markup' => $markup,
							'text' => $text,
						]);
					} else {
						$this->telegram->sendMessage([
							'chat_id' => $chat_id,
							'text' => 'Напишіть назву товару: ',
						]);
					}
				}
				break;
			case 'type.shelter':
			case 'type.zoo':
				$param = $command;
				$res = GetCommand::getRequestCommand('setContractor', $param, $message);
				$this->telegram->sendMessage([
					'chat_id' => $chat_id,
					'text' => 'Скорочена назва товару. ' . "\n" . '[Наприклад: Китикет]',
				]);
				break;
			case 'setPurpose':
			case 'enterOther':
			case 'tovarYes':
				$res = GetCommand::getRequestCommand($command, $param, $message);
				$this->telegram->sendMessage([
					'chat_id' => $chat_id,
					'text' => 'Скорочена назва товару. ' . "\n" . '[Наприклад: Китикет]',
				]);
				break;
			case 'Exit':
			case 'tovarNo':
				$res = GetCommand::getRequestCommand('Exit', '-', $message);
				if ($command == 'Exit') {
					$text = 'АСТАНАВИТЕСЬ!';
				} else {
					if ($checkUser[0]['userTypeID'] == UserType::volonter) {
						$text = 'Ваша заявка оформлена та буде передана благодійним організаціям. У разі наявності товару,
					 з Вами зв`яжуться.' . "\n\n" . 'Дякуємо за участь в проекті.' . "\n\n" . "<b>Слава Україні! </b>" .
							"\xF0\x9F\x92\x99 \xF0\x9F\x92\x9B";
					} else {
						$text = 'Ваша пропозиція успішно додана.' . " \xE2\x9C\x85 " . " \n\n " .
							'Дякуємо за участь в проекті.' . " \n\n " .
							"<b>Слава Україні! </b> " . "\xF0\x9F\x92\x99 \xF0\x9F\x92\x9B";
					}
				}
				$this->telegram->sendMessage([
					'chat_id' => $chat_id,
					'parse_mode' => 'HTML',
					'text' => $text,
					'reply_markup' => self::renderExitKeyboard()
				]);
				break;
			case 'Contractor':
				if ($checkUser[0]['param'] == 'exelVolonterNo' || $checkUser[0]['command'] == 'setRegion') {
					$res = GetCommand::getRequestCommand($command, 'Contractor', $message);
					$this->telegram->sendMessage([
						'chat_id' => $chat_id,
						'text' => 'Будь ласка, введіть ЄДРПОУ організаці/заявника: ',
					]);
				}
				break;
			case 'defaultCommand':
			default:
				if ($checkUser) {
					switch ($checkUser[0]['command']) {
						case 'getResult':
						case 'setPurpose':
						case 'getFood':
						case 'setResult':
						case 'setFilter':
						case 'tovarYes':
						case 'enterOther':
						case 'Contractor':
						case 'setContractor':
						case 'setContractor.edrpou':
						case 'setContractor.name':
						case 'setContractor.address':
							switch ($checkUser[0]['userTypeID']) {
								case UserType::searcher:
									if ((new CheckAnimalFood())->checkAnimalFood($message['text'])) {
										$message['text'] = '*';
									}
									$res = [];
									if ($checkUser[0]['command'] !== 'getResult') {
										$res = GetCommand::getRequestCommand($command, $param, $message);
									}
									if ($res) {
										if ($res[0]['count'] > 20 && $res[0]['type'] == 'queue') {
											$text = 'По вашому запиту ' . $res[0]['count'] . '. Очікуйте відопідь у PDF форматі.';
											$executor = Container::get(RabbitMQ::class);
											$res[0] += ['telegramId' => $chat_id];
											$executor->send(
												Container::get('config')['RabbitMq']['default_mq']['queue'],
												json_encode($res)
											);
										} else {
											$text = 'Повашому запросу корм мають: ' . "\n\n";
											foreach ($res as $elem) {
												$text .= '|' . "<b>" . $elem['userName'] . ' з контактами: ' . $elem['contacts'] .
													' з продукцією: ' . $elem['food'] . " " . "</b>" . "\xF0\x9F\x99\x8B"
													. '|' . "\n\n";
											}
											$text .= 'Дякуємо що скористувались нашим ботом. ' . "\n" . 'Слава Україні!';
										}
										$this->telegram->sendMessage([
											'chat_id' => $chat_id,
											'parse_mode' => 'HTML',
											'text' => $text,
											'reply_markup' => self::renderExitKeyboard()
										]);
										$res = GetCommand::getRequestCommand('Exit', '-', $message);
									} else {
										if ($checkUser[0]['command'] === 'getResult') {
											$param = 'description';
											$res = GetCommand::getRequestCommand($command, $param, $message);
											$this->telegram->sendMessage([
												'chat_id' => $chat_id,
												'text' => 'Дякуємо за використання.',
												'reply_markup' => self::renderExitKeyboard()
											]);
											$res = GetCommand::getRequestCommand('Exit', '-', $message);
										} else {
											$this->telegram->sendMessage([
												'chat_id' => $chat_id,
												'text' => 'По вашому запиту нічого не знайдено. Введіть потребу. Ми
											 надішлемо запит на вашу потребу. Детальний опис товару. ' . "\n" .
													'[Фасування, ціна, додатковий опис]'
											]);
										}
									}
									break;
								case UserType::volonter:
									$temp = true;
									$command = 'setContractor';
									switch ($checkUser[0]['param']) {
										case 'Contractor':
											$param = 'edrpou';
											$text = 'Будь ласка, введіть Назву організаці/заявника: ';
											break;
										case 'edrpou':
											$param = 'name';
											$text = 'Будь ласка, введіть Адресу організаці/заявника: ';
											break;
										case 'name':
											$param = 'address';
											$text = 'Вкажіть тип орагнізації/заявника:';
											break;
										case 'type':
											$temp = false;
											$param = 'tovar';
											$text = 'Детальний опис товару. ' . "\n" . '[Фасування, ціна, додатковий опис]';
											$this->telegram->sendMessage([
												'chat_id' => $chat_id,
												'parse_mode' => 'HTML',
												'text' => $text,
											]);
											break;
										case 'tovar':
											$temp = false;
											$param = 'description';
											$markup = Keyboard::make([
												'inline_keyboard' => self::renderButtonsSelect(
													'tovarYes',
													'tovarNo',
													'Додати',
													'Вихід'
												),
												'resize_keyboard' => true,
												'one_time_keyboard' => false
											]);
											$this->telegram->sendMessage([
												'chat_id' => $chat_id,
												'text' => 'Бажаєте додати новий товар?',
												'reply_markup' => $markup,
											]);
											break;
									}
									$res = GetCommand::getRequestCommand($command, $param, $message);
									if ($param == 'address') {
										$count = count($res);
										for ($i = 0; $i < $count; $i++) {
											$res[$i]['code'] = 'type.' . $res[$i]['code'];
										}
										$markup = Keyboard::make([
											'inline_keyboard' => self::renderTwoCollKeyboard($res),
											'resize_keyboard' => true,
											'one_time_keyboard' => false,
										]);
										$this->telegram->sendMessage([
											'chat_id' => $chat_id,
											'text' => $text,
											'reply_markup' => $markup
										]);
									} else {
										if ($temp) {
											$this->telegram->sendMessage([
												'chat_id' => $chat_id,
												'text' => $text,
											]);
										}
									}
									break;
								case UserType::saler:
									if ($checkUser[0]['param'] == 'tovar') {
										$param = 'description';
										$markup = Keyboard::make([
											'inline_keyboard' => self::renderButtonsSelect(
												'tovarYes',
												'tovarNo',
												'Додати',
												'Вихід'
											),
											'resize_keyboard' => true,
											'one_time_keyboard' => false
										]);
										$this->telegram->sendMessage([
											'chat_id' => $chat_id,
											'text' => 'Бажаєте додати новий товар?',
											'reply_markup' => $markup,
										]);
									} else {
										$param = 'tovar';
										$text = 'Детальний опис товару. ' . "\n" . '[Фасування, ціна, додатковий опис]';
										$this->telegram->sendMessage([
											'chat_id' => $chat_id,
											'parse_mode' => 'HTML',
											'text' => $text,
										]);
									}
									$res = GetCommand::getRequestCommand($command, $param, $message);
									break;
							}
							break;
						case 'setRegion':
							/**
							 * Команда реєстрації
							 */
							$command = 'setContact';
							$res = GetCommand::getRequestCommand($command, $param, $message);
							$markup = Keyboard::make([
								'inline_keyboard' => self::renderTwoCollKeyboard($res),
								'resize_keyboard' => true,
								'one_time_keyboard' => false
							]);
							$this->telegram->sendMessage([
								'chat_id' => $chat_id,
								'text' => 'Оберіть вид тварини. ' . "\xF0\x9F\x98\xBC \xF0\x9F\x90\xB6",
								'reply_markup' => $markup
							]);
							break;
					}
				} else {
					$res = GetCommand::getRequestCommand('Exit', '-', $message);
					$this->telegram->sendMessage([
						'chat_id' => $chat_id,
						'parse_mode' => 'HTML',
						'text' => "По запросу \"<b>" . $text . "</b>\" нічого не знайдено." . "\xF0\x9F\x99\x85",
						'reply_markup' => self::renderExitKeyboard()
					]);
				}
				break;
		}
	}

	/**
	 * @return Keyboard
	 */
	public static function renderExitKeyboard(): Keyboard
	{
		return Keyboard::make([
			'inline_keyboard' => [
				[
					Keyboard::inlineButton(['callback_data' => '/start', 'text' => 'На початок']),
				]
			],
			'resize_keyboard' => true,
			'one_time_keyboard' => false,
		]);
	}

	/**
	 * @param array $data
	 * @return array
	 */
	public static function renderTwoCollKeyboard(array $data): array
	{
		$board = [];
		$count = count($data);
		switch ($count % 2) {
			case 0:
				for ($i = 0; $i < $count; $i = $i + 2) {
					$board[] = [
						Keyboard::inlineButton(['callback_data' => $data[$i]['Code'], 'text' => $data[$i]['Name']]),
						Keyboard::inlineButton(['callback_data' => $data[$i + 1]['Code'], 'text' => $data[$i + 1]['Name']])
					];
				}
				break;
			case 1:
				for ($i = 0; $i < $count; $i = $i + 2) {
					if ($count == $i + 1) {
						$i = $i - 1;
						$board[] = [
							Keyboard::inlineButton(['callback_data' => $data[$i + 1]['Code'], 'text' => $data[$i + 1]['Name']]),
						];
					} else {
						$board[] = [
							Keyboard::inlineButton(['callback_data' => $data[$i]['Code'], 'text' => $data[$i]['Name']]),
							Keyboard::inlineButton(['callback_data' => $data[$i + 1]['Code'], 'text' => $data[$i + 1]['Name']])
						];
					}
				}
				break;
		}
		return $board;
	}

	/**
	 * @param array $data
	 * @return array
	 */
	public static function renderThreeCollKeyboard(array $data): array
	{
		$board = [];
		$count = count($data);
		switch ($count % 3) {
			case 0:
				for ($i = 0; $i < $count; $i = $i + 3) {
					$board[] = [
						Keyboard::inlineButton(['callback_data' => $data[$i]['Code'], 'text' => $data[$i]['Name']]),
						Keyboard::inlineButton(['callback_data' => $data[$i + 1]['Code'], 'text' => $data[$i + 1]['Name']]),
						Keyboard::inlineButton(['callback_data' => $data[$i + 2]['Code'], 'text' => $data[$i + 2]['Name']])
					];
				}
				break;
			case 1:
				for ($i = 0; $i < $count; $i = $i + 3) {
					if ($count == $i + 1) {
						$i = $i - 1;
						$board[] = [
							Keyboard::inlineButton(['callback_data' => $data[$i + 1]['Code'], 'text' => $data[$i + 1]['Name']]),
						];
					} else {
						$board[] = [
							Keyboard::inlineButton(['callback_data' => $data[$i]['Code'], 'text' => $data[$i]['Name']]),
							Keyboard::inlineButton(['callback_data' => $data[$i + 1]['Code'], 'text' => $data[$i + 1]['Name']]),
							Keyboard::inlineButton(['callback_data' => $data[$i + 2]['Code'], 'text' => $data[$i + 2]['Name']])
						];
					}
				}
				break;
			case 2:
				for ($i = 0; $i < $count; $i = $i + 3) {
					if ($count == $i + 2) {
						$board[] = [
							Keyboard::inlineButton(['callback_data' => $data[$i]['Code'], 'text' => $data[$i]['Name']]),
							Keyboard::inlineButton(['callback_data' => $data[$i + 1]['Code'], 'text' => $data[$i + 1]['Name']]),
						];
					} else {
						$board[] = [
							Keyboard::inlineButton(['callback_data' => $data[$i]['Code'], 'text' => $data[$i]['Name']]),
							Keyboard::inlineButton(['callback_data' => $data[$i + 1]['Code'], 'text' => $data[$i + 1]['Name']]),
							Keyboard::inlineButton(['callback_data' => $data[$i + 2]['Code'], 'text' => $data[$i + 2]['Name']])
						];
					}
				}
				break;
		}
		return $board;
	}

	/**
	 * @param string $positiveCode
	 * @param string $negativeCode
	 * @param string $positiveText
	 * @param string $negativeText
	 * @return array[]
	 */
	public static function renderButtonsSelect(
		string $positiveCode, string $negativeCode,
		string $positiveText, string $negativeText
	): array
	{
		return [
			[
				Keyboard::inlineButton(['callback_data' => $positiveCode, 'text' => $positiveText]),
				Keyboard::inlineButton(['callback_data' => $negativeCode, 'text' => $negativeText])
			]
		];
	}

}
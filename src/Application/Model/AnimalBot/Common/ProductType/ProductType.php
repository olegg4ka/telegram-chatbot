<?php

namespace Application\Model\AnimalBot\Common\ProductType;

/**
 *
 */
class ProductType
{
	const Food = 'Food'; //Корми
	const VetDrug = 'VetDrug';  //Ветеринарні препарати
	const Other = 'Other';    //Інші продукти
}
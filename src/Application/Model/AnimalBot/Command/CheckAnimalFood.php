<?php

namespace Application\Model\AnimalBot\Command;

/**
 * Перевірка вводу кормів тварин
 */
class CheckAnimalFood
{
	/**
	 * @param string $text
	 * @return bool
	 */
	public function checkAnimalFood(string $text): bool
	{
		switch ($text) {
			case 'Любий корм':
			case 'Любі корма':
			case 'Корм':
			case 'Корма':
			case 'Корми':
			case 'Кормы':
			case 'Любой корм':
				return true;
			default:
				return false;
		}
	}

}
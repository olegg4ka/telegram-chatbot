<?php

namespace Application\Model\AnimalBot\Command;


/**
 * Commands
 */
class Command
{
	/**
	 * @var string
	 */
	private $command;

	/**
	 * @param string $name
	 */
	public function __construct(string $name)
	{
		$this->command = $name;
	}

	/**
	 * @return string
	 */
	public function getCommand(): string
	{
		switch ($this->command) {
			case 'старт':
			case 'Старт':
			case 'start':
			case 'Start':
			case '/start':
				return 'Start';
			case '/help':
				return 'Help';
			case 'setContact':
				return 'setContact';
			case 'enterRegister':
				return 'enterRegister';
			case "Helping":
				return 'Helping';
			case "Search":
				return 'Search';
			case 'Volonter':
				return 'Volonter';
			case 'Frankovsk':
			case 'Krim':
			case 'Vinnica':
			case 'Volin':
			case 'Dnipro':
			case 'Doneck':
			case 'Zhitomir':
			case 'Zakarpatya':
			case 'Zaporizha':
			case 'Kirivogrd':
			case 'Kyivska':
			case 'Lugansk':
			case 'Lviv':
			case 'Kyiv':
			case 'Sevastopol':
			case 'Mikolaiv':
			case 'Odessa':
			case 'Poltava':
			case 'Povno':
			case 'Sumi':
			case 'Ternoil':
			case 'Kharkiv':
			case 'Kherson':
			case 'Khmelnickiy':
			case 'Cherkasy':
			case 'Chernivci':
			case 'Chernigiv':
				return 'setRegion';
			case "Cat":
			case "Dog":
			case "OtherAnimal":
				return 'setAnimal';
			case 'Food':
			case 'VetDrug':
			case 'Other':
				return 'setProductType';
			case 'catFood1':
			case 'catFood2':
			case 'catFood3':
			case 'catFood4':
			case 'catFood5':
			case 'dogFood1':
			case 'dogFood2':
			case 'dogFood3':
			case 'dogFood4':
			case 'dogFood5':
			case 'dogFood6':
			case 'catProd1':
			case 'catProd2':
			case 'catProd3':
			case 'catProd4':
			case 'catProd5':
			case 'dogProd1':
			case 'dogProd2':
			case 'dogProd3':
			case 'dogProd4':
			case 'dogProd5':
			case 'dogProd6':
			case 'catDrug31':
			case 'catDrug32':
			case 'catDrug33':
			case 'catDrug34':
			case 'catDrug35':
			case 'catDrug36':
			case 'catDrug37':
			case 'catDrug38':
			case 'catDrug39':
			case 'catDrug40':
			case 'catDrug41':
			case 'catDrug42':
			case 'catDrug43':
			case 'catDrug44':
			case 'catDrug45':
			case 'catDrug46':
			case 'catDrug47':
			case 'catDrug48':
			case 'catDrug49':
			case 'catDrug50':
			case 'catDrug51':
			case 'catDrug52':
			case 'catDrug53':
			case 'catDrug54':
			case 'catDrug55':
			case 'catDrug56':
			case 'catDrug57':
			case 'catDrug58':
			case 'catDrug59':
			case 'catDrug60':
			case 'catDrug61':
			case 'catDrug62':
			case 'dogDrug31':
			case 'dogDrug32':
			case 'dogDrug33':
			case 'dogDrug34':
			case 'dogDrug35':
			case 'dogDrug36':
			case 'dogDrug37':
			case 'dogDrug38':
			case 'dogDrug39':
			case 'dogDrug40':
			case 'dogDrug41':
			case 'dogDrug42':
			case 'dogDrug43':
			case 'dogDrug44':
			case 'dogDrug45':
			case 'dogDrug46':
			case 'dogDrug47':
			case 'dogDrug48':
			case 'dogDrug49':
			case 'dogDrug50':
			case 'dogDrug51':
			case 'dogDrug52':
			case 'dogDrug53':
			case 'dogDrug54':
			case 'dogDrug55':
			case 'dogDrug56':
			case 'dogDrug57':
			case 'dogDrug58':
			case 'dogDrug59':
			case 'dogDrug60':
			case 'dogDrug61':
			case 'dogDrug62':
				return 'setProductSubType';
			case 'catGroupAll':
			case 'catGroup2':
			case 'catGroup3':
			case 'catGroup4':
			case 'catGroup5':
			case 'dogGroupAll':
			case 'dogGroup2':
			case 'dogGroupBreed':
			case 'dogGroupBig':
			case 'dogGroupLittle':
			case 'dogGroupMean':
				return 'setAnimalGroup';
			case 'catBreed1':
			case 'catBreed2':
			case 'catBreed3':
			case 'catBreed4':
			case 'dogBreed1':
			case 'dogBreed2':
			case 'dogBreed3':
			case 'dogBreed4':
			case 'dogBreed5':
			case 'dogBreed6':
			case 'dogBreed7':
			case 'dogBreed8':
			case 'dogBreed9':
				return 'setBreed';
			case 'catPurp1':
			case 'catPurp2':
			case 'catPurp3':
			case 'catPurp4':
			case 'catPurp5':
			case 'catPurp6':
			case 'catPurp7':
			case 'catPurp8':
			case 'catPurp9':
			case 'catPurp10':
			case 'catPurp11':
			case 'catPurp12':
			case 'catPurp13':
			case 'catPurp14':
			case 'catPurp15':
			case 'catPurp16':
			case 'catPurp17':
			case 'catPurp18':
			case 'catPurp19':
			case 'catPurp21':
			case 'catPurp22':
			case 'catPurp23':
			case 'catPurp24':
			case 'catPurp25':
			case 'catPurp26':
			case 'catPurp27':
			case 'catPurp28':
			case 'catPurp29':
			case 'catPurp30':
			case 'catPurp31':
			case 'catPurpAll':
			case 'catPurp32' :
			case 'catPurp33' :
			case 'catPurp34' :
			case 'catPurp35' :
			case 'catPurp36' :
			case 'catPurp37' :
			case 'catPurp38' :
			case 'catPurp39' :
			case 'catPurp40' :
			case 'catPurp41' :
			case 'catPurp42' :
			case 'catPurp43' :
			case 'catPurp44' :
			case 'catPurp45' :
			case 'catPurp46' :
			case 'catPurp47' :
			case 'catPurp48' :
			case 'catPurp49' :
			case 'catPurp50' :
			case 'catPurp51' :
			case 'catPurp52' :
			case 'catPurpOther' :
			case 'catPurp53' :
			case 'catPurp54' :
			case 'dogPurp1':
			case 'dogPurp2':
			case 'dogPurp3':
			case 'dogPurp4':
			case 'dogPurp5':
			case 'dogPurp6':
			case 'dogPurp7':
			case 'dogPurp8':
			case 'dogPurp9':
			case 'dogPurp10':
			case 'dogPurp11':
			case 'dogPurp12':
			case 'dogPurp13':
			case 'dogPurp14':
			case 'dogPurp15':
			case 'dogPurp16':
			case 'dogPurp17':
			case 'dogPurp18':
			case 'dogPurp19':
			case 'dogPurp20':
			case 'dogPurp21':
			case 'dogPurp22':
			case 'dogPurp23':
			case 'dogPurp24':
			case 'dogPurp25':
			case 'dogPurp26':
			case 'dogPurp27':
			case 'dogPurp28':
			case 'dogPurp29':
			case 'dogPurp30':
			case 'dogPurp31':
			case 'dogPurp32':
			case 'dogPurp33':
			case 'dogPurp34':
			case 'dogPurp35':
			case 'dogPurp36':
			case 'dogPurp37':
			case 'dogPurp38':
			case 'dogPurp39':
			case 'dogPurp40':
			case 'dogPurp41':
			case 'dogPurp42':
			case 'dogPurp43':
			case 'dogPurp44':
			case 'dogPurp45':
			case 'dogPurp46':
			case 'dogPurp47':
			case 'dogPurp48':
			case 'dogPurp49':
			case 'dogPurp50':
			case 'dogPurp51':
			case 'dogPurp52':
			case 'dogPurp53':
				return 'setPurpose';
			case 'catOther1':
			case 'catOther2':
			case 'catOther3':
			case 'catOther4':
			case 'catOther5':
			case 'catOther6':
			case 'catOther7':
			case 'catOther8':
			case 'catOther9':
			case 'catOther10':
			case 'catOther11':
			case 'catOther12':
			case 'catOther13':
			case 'dogOther1':
			case 'dogOther2':
			case 'dogOther3':
			case 'dogOther4':
			case 'dogOther5':
			case 'dogOther6':
			case 'dogOther7':
			case 'dogOther8':
			case 'dogOther9':
			case 'dogOther10':
			case 'dogOther11':
			case 'dogOther12':
			case 'dogOther13':
			case 'dogOther14':
			case 'dogOther15':
			case 'dogOther16':
				return 'enterOther';
			case 'setFilterYes':
			case 'setFilterNo':
				return 'setFilter';
			case 'setPurpose':
			case 'enterOther':
			case '*':
			default:
				return 'defaultCommand';
			case '/exit':
			case 'isExitYes':
				return 'Exit';
			case 'tovarNo':
				return 'tovarNo';
			case 'tovarYes':
				return 'tovarYes';
			case 'isExitNo':
				return 'Continue';
			case 'document':
				return 'document';
			case 'exelVolonterYes':
			case 'exelYes':
				return 'documentInfo';
			case 'getExample':
				return 'getExample';
			case 'setFile';
				return 'setFile';
			case 'exelNo':
				return 'setRegionContinue';
			case 'exelVolonterNo':
				return 'Contractor';
			case 'zoo':
				return 'type.zoo';
			case 'shelter':
				return 'type.shelter';
		}
		return $this->command;
	}

}